﻿using IPConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.IO;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net.Http;

namespace RTCCInstaller
{
    public partial class frmMain : Form
    {



        private static Dictionary<IPAddress, NetworkInfoObject> ClientNetwork = new Dictionary<IPAddress, NetworkInfoObject>();

        const string NONE_FOUND = "None Found";

        private static volatile int NetworkIndex = 0;

        const string CLASS_A = "255.0.0.0";
        const string CLASS_B = "255.255.0.0";
        const string CLASS_C = "255.255.255.0";

        private static string[] SUBNETS = { CLASS_C, CLASS_B, CLASS_A };

        private int ipmod = 1;
        private volatile int fcount = 1;
        private static volatile bool IsNetworkUpdating = false;

        private PingManager pinger;
        private Thread networkInfoThread;
        private Thread nabSearchThread;

        public bool IsMultiGateway { get; private set; }
        public NetworkInfoObject CurrentNetwork { get; private set; }
        public bool IngenicoDriversDetected { get; private set; }
        public bool IsRunningElavonInstaller { get; private set; } = false;

        public volatile bool IsWizard = false;

        private ManualResetEvent threadEvent;
        private List<InstalledProgram> OldDriverList;

        private volatile int MaxItemsToUninstall = 0;
        private volatile int UninstallCount = 0;
        const string RTPOS_FOLDER = "RealtimePOS";
        const string ELAVON_UNINSTALL_EXE = "unins000.exe";

        const string ELAVON = "Elavon";
        const string CONVERAGE_CONNECT = "ConvergeConnect";
        const string C_C_ADMIN_EXE = "ConvergeConnectAdmin.exe";

        public frmMain()
        {
            InitializeComponent();           
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            IsRunningElavonInstaller = false;
            Program.SetText(lblGateway, "");
            Program.SetText(lblSubnet, "");
            Program.SetText(lblDns1, "");
            Program.SetText(lblDns2, "");
            IsMultiGateway = false;
            Program.SetProgress(progBarNab, false);


            conConnectTimer = new System.Timers.Timer(1200);
            conConnectTimer.Elapsed += ConConnectTimer_Elapsed;
            conConnectTimer.Start();

        }

        private void ConConnectTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (Process.GetProcessesByName(
                        C_C_ADMIN_EXE.Replace(".exe", string.Empty).Trim()).Length > 0)
                Program.SetVisible(lblConverageStatus, true);
            else
                Program.SetVisible(lblConverageStatus, false);
        }

        private void LstActiveDevices_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            


            var selIndex = this.lstActiveDevices.IndexFromPoint(e.Location);
            if (selIndex == ListBox.NoMatches)
                return;

            var selItem = this.lstActiveDevices.SelectedItem.ToString();
            if (selItem.Equals(NONE_FOUND))
                return;
            

            var devices = ActiveDeviceStore.Keys;
            NabObject nabObj = devices.ToArray()[selIndex];
            var nabEvent = new NabEventArgs();
            if (ActiveDeviceStore.TryGetValue(nabObj, out nabEvent))
            {
                List<string> data = new List<string>();
                data.Add(string.Format("{0}", nabEvent.ResultText));
                data.Add(Environment.NewLine);
                data.Add(string.Format("IP Address: {0}", nabObj.Address.ToString()));
                data.Add(Environment.NewLine);
                data.Add(string.Format("RTPOS IP String:{1}http://{0}:6200", 
                    nabObj.Address.ToString(), Environment.NewLine));
                data.Add(Environment.NewLine);
                data.Add(string.Format("Merchant ID: {0}", nabObj.MerchantNumber));
                data.Add(string.Format("Terminal #: {0}", nabObj.TerminalNumber));
                data.Add(Environment.NewLine);
                data.Add(string.Format("Response: {0}", nabObj.Response));
                data.Add(string.Format("Response Text: {0}", nabObj.ResponseText));

                new frmDisplay(data.ToArray()).ShowDialog();
                
            }

        }

        private void btnGetNetwork_Click(object sender, EventArgs e)
        {
            Program.SetVisible(lblNote, true);
            Program.SetForground(lblNote, Color.Blue);

            UpdateNetworkInfo();
        }

        private void UpdateNetworkInfo()
        {

            networkInfoThread = new Thread(new ThreadStart(GetNetworkInfo));
            Program.ToDispose(networkInfoThread);
            networkInfoThread.Start();

            portTestThread = new Thread(new ThreadStart(RunPortTest));
            Program.ToDispose(portTestThread);
            portTestThread.Start();
        }

        private const int portNabCom = 9999;
        private const int portNabUpdate = 2600;
        private const int portPosLocal = 6200;

        private async void RunPortTest()
        {

            if (CheckInternalPort(portPosLocal))
                Program.SetForground(lblPOSLocal, Color.Green);
            else
                Program.SetForground(lblPOSLocal, Color.Red);


            if (await CheckOutgoingPort(portNabUpdate))
                Program.SetForground(lblNabUpdate, Color.Green);
            else
                Program.SetForground(lblNabUpdate, Color.Red);

            if (await CheckOutgoingPort(portNabCom))
                Program.SetForground(lblNabCom, Color.Green);
            else
                Program.SetForground(lblNabCom, Color.Red);

           

        }

        const string portQuiz = "http://portquiz.net";

        private async Task<bool> CheckOutgoingPort(int port)
        {
            return await CheckPort(portQuiz, port);
        }

        private async Task<bool> CheckPort(string host, int port)
        {

            var successCount = 0;
            host = string.Format("{0}:{1}", host, port);
            using(var client = new HttpClient())
            {
                var req = new FormUrlEncodedContent(new[] 
                    { new KeyValuePair<string, string>("port", port.ToString()) });

                var rep = await client.PostAsync(host, req);

                var result = rep.Content;
               
                using (var reader = new StreamReader(await result.ReadAsStreamAsync()))
                {

                   var text = await reader.ReadToEndAsync();
                    var textLines = text.Split('\n');
                   
                    foreach(var line in textLines)
                    {
                        //Console.WriteLine(line);
                        var template = string.Format("You have reached this page on port <b>{0}</b>.<br/>", port);
                        if (line.Equals(template))
                            successCount++;

                        if (line.Equals("Your network allows you to use this port."))
                            successCount++;
                        
                    }
                    
                   
                }

            }

            return successCount == 2;
        }

        private bool CheckInternalPort(int port)
        {            
            var isAvailable = true;
            
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

            foreach (TcpConnectionInformation tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port == port)
                {
                    isAvailable = false;
                    break;
                }
            }

            return isAvailable;
        }

        private void GetNetworkInfo()
        {
            
            SetProgress(progBarNab, true);
            ClientNetwork = new Dictionary<IPAddress, NetworkInfoObject>();

            foreach (var neti in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (neti == null || neti.OperationalStatus != OperationalStatus.Up)
                    continue;

                var ipprops = neti.GetIPProperties();
                var gateways = ipprops.GatewayAddresses;
                var addr = IPAddress.Loopback;
                foreach(var gw in gateways)
                {
                    if (gw == null || gw.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
                        continue;

                    var network = new NetworkInfoObject();
                    network.GatewayAddress = gw.Address;
                    foreach(var dnsaddr in ipprops.DnsAddresses)
                    {
                        if (dnsaddr.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
                            continue;

                        network.DnsAddresses.Add(dnsaddr);                        
                    }
                    ClientNetwork.Add(gw.Address, network);
                }
                
            }

            if(ClientNetwork.Count > 0)
            {
                if (ClientNetwork.Count > 1)
                    IsMultiGateway = true;
                
                foreach (var gateway in ClientNetwork.Keys)
                {
                    var network = new NetworkInfoObject();
                    if (gateway.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        var lIndex = gateway.ToString().LastIndexOf('.');
                        var leadAddr = gateway.ToString().Substring(0, lIndex);

                        pinger = new PingManager(leadAddr);
                        pinger.PingAll();

                        Thread.Sleep(100);

                        while (pinger.IsWorking)
                        {
                            Thread.Sleep(65);
                        }
                        var allActive = pinger.GetActive();
                        if(allActive.Count > 0)
                        {
                            foreach(var adrs in allActive)
                            {
                                if (adrs != null)
                                {
                                    
                                    ClientNetwork.TryGetValue(gateway, out network);
                                    network.ActiveAddresses.Add(adrs);
                                }
                                                                 
                            }
                        }

                        for(var i = 0; i < 6; i++)
                        {
                            var rawip = IPAddress.Loopback;
                            var found = GetUnusedValidIp(gateway, leadAddr, out rawip);
                            while (network.ActiveAddresses.Contains(rawip))
                            {
                                found = GetUnusedValidIp(gateway, leadAddr, out rawip);
                            }
                            if (found)
                                network.FreeAddresses.Add(rawip);
                        }

                        if(network.FreeAddresses.Count > 0)
                        {
                            foreach(var ip in network.FreeAddresses)
                            {
                                var subnet = GetSubnetMask(gateway, ip);
                                if(subnet != IPAddress.Loopback)
                                    network.ValidSubnets.Add(subnet);
                            }
                        }
                        else
                        {
                            HandleError(string.Format("{0}{2}{1}", "No Free Addresses Found for Broadcast {3} - Possible Network Issues/Intranet/Invalid Setup", 
                                "Could also mean something is stopping this program from Pinging", Environment.NewLine, network.GatewayAddress.ToString()));
                        }
                    }
                }

                UpdateGUI(NetworkIndex);


            }
            else
            {
                HandleError("No Gateways Found/No IPv4 Gateway address found - Try DHCP and not static IP");
            }

            if (CurrentNetwork != null)
                Program.SetEnabled(nABEPXToolStripMenuItem, true);
        }

        private void SetProgress(ProgressBar progBarNab, bool start)
        {
            IsNetworkUpdating = start;
            Cursor.Current = start ? Cursors.WaitCursor : Cursors.Default;
            Application.DoEvents();
            Program.SetProgress(progBarNab, start);
        }

        private void UpdateGUI(int index)
        {
            var cnet = new NetworkInfoObject();
            SetProgress(progBarNab, true);
            if (IsMultiGateway)
            {
                MessageBox.Show(null,
                        string.Format("Found {0} Possible valid Gateways? Use Next Gateway to Cycle valid Broadcast Addresses", ClientNetwork.Count),
                        "Multiple Gateways",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                Program.SetVisible(btnNext, true);
                
                
            }
                

            if(ClientNetwork.TryGetValue(ClientNetwork.Keys.ToArray()[index], out cnet))
            {
                CurrentNetwork = cnet;

                Program.SetText(lblGatewayLabel, string.Format("Gateway ({0}/{1})", (NetworkIndex+1), ClientNetwork.Count));
                Program.SetForground(lblGatewayLabel, Color.ForestGreen);

                Program.SetText(lblGateway, CurrentNetwork.GatewayAddress.ToString());
                Program.SetForground(lblGateway, Color.Green);

                Program.SetText(lblSubnet, CurrentNetwork.ValidSubnets.First().ToString());
                Program.SetForground(lblSubnet, Color.Green);

                Program.SetText(lblDns1, CurrentNetwork.DnsAddresses[0].ToString());
                Program.SetForground(lblDns1, Color.Green);
                if (CurrentNetwork.DnsAddresses.Count > 1)
                    Program.SetText(lblDns2, CurrentNetwork.DnsAddresses[1].ToString());
                else
                    Program.SetText(lblDns2, CurrentNetwork.DnsAddresses[0].ToString());
                Program.SetForground(lblDns2, Color.Green);

                if (CurrentNetwork.FreeAddresses.Count > 0)
                {

                    string[] freeips = new string[CurrentNetwork.FreeAddresses.Count];
                    for(var i = 0; i < CurrentNetwork.FreeAddresses.Count; i++)
                    {
                        freeips[i] = CurrentNetwork.FreeAddresses[i].ToString();
                    }

                    Program.ClearItems(lstOpen);
                    Program.SetItems(lstOpen, freeips);

                    
                }

                if (CurrentNetwork.ActiveAddresses.Count > 0)
                {
                   // Program.SetEnabled(btnNabSearch, true);
                    List<string> activeIPAddresses = new List<string>();
                    foreach (var activeip in CurrentNetwork.ActiveAddresses)
                        activeIPAddresses.Add(string.Format("{0}{1}", activeip.ToString(), Environment.NewLine));

                    Program.SetItems(lstActive, activeIPAddresses.ToArray());
                    Program.SetForground(lstActive, Color.DarkRed);

                    Program.SetText(lblActiveIPCount, CurrentNetwork.ActiveAddresses.Count.ToString());
                    Program.SetForground(lblActiveIPCount, System.Drawing.Color.Green);
                }
                else
                {
                    Program.SetText(lblActiveIPCount, "0");
                    Program.SetForground(lblActiveIPCount, System.Drawing.Color.Red);
                }

            } else
            {
                HandleError("Problem with Network Address");
            }


            SetProgress(progBarNab, false);
        }

        private IPAddress GetSubnetMask(IPAddress gateway, IPAddress address)
        {
            foreach(var sn in SUBNETS)
            {
                var snip = IPAddress.Parse(sn);
                if (gateway.IsInSameSubnet(address, snip))
                    return snip;
            }
            return IPAddress.Loopback;
        }

        private bool GetUnusedValidIp(IPAddress addr, string leadAddr, out IPAddress rawip)
        {
            rawip = IPAddress.Loopback;
            var endip = GetRandom(GetSeed());
            var sb = new StringBuilder();
            sb.Append(leadAddr);
            sb.Append(".");
            sb.Append(endip);
            var nip = sb.ToString();
            if(IPAddress.TryParse(nip, out rawip))
            {
                if (IsValidIp(addr, rawip))
                    return true;
                else
                    GetUnusedValidIp(addr, leadAddr, out rawip);
            }
            return false;
        }

        private object GetRandom(int seed)
        {
            var r = (new Random(seed).Next(5, 251)) + ipmod++;
            while(r > 253)
            {
                r = (new Random(seed).Next(5, 251)) - ipmod++;
                if (ipmod >= 255)
                    ipmod = 1;
            }
            return r;
        }

        private static bool IsValidIp(IPAddress gateway, IPAddress raw)
        {
            var vcnt = 0;
            var gateOctets = gateway.ToString().Split('.');
            var rawOctets = raw.ToString().Split('.');
            for(var i = 0; i < 3; i++)
            {
                if (gateOctets[i] == rawOctets[i])
                    vcnt++;
            }
            return (vcnt == 3);
        }

        private int GetSeed()
        {
            return Environment.TickCount + DateTime.Now.Millisecond + DateTime.Now.Second + DateTime.Now.Minute;
        }

        private void HandleError(string v)
        {
            var res = MessageBox.Show(null, v, "Network Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            if (res == DialogResult.Abort)
                Program.Shutdown();
            if(res == DialogResult.Retry)
            {                
                GetNetworkInfo();
            }
            if (res == DialogResult.Ignore)
            {
                //ignore
            }

        }

        

        private void btnNext_Click(object sender, EventArgs e)
        {
            if(ClientNetwork.Count > 1)
            {
                NetworkIndex++;
                if (NetworkIndex >= ClientNetwork.Count)
                    NetworkIndex = 0;

                UpdateGUI(NetworkIndex);
            }
        }

        private void btnNabSearch_Click(object sender, EventArgs e)
        {
            if (Program.IsNabSearchUsed)
                NabSearch.Instance.Reset();

            new Thread(() =>
            {
                Program.ClearItems(lstActiveDevices);
                SetProgress(progBarNab, true);
                UpdateNetworkInfo();
                while (IsNetworkUpdating)
                {
                    Thread.Sleep(50);
                }
                Task.Delay(100).Wait();
                nabSearchThread = new Thread(new ThreadStart(FindNabDevices));
                Program.ToDispose(nabSearchThread);
                nabSearchThread.Start();

            }).Start();

            

            
        }

        private void FindNabDevices()
        {
            Program.SetProgress(progBarNab, true);
            NabSearch.Instance.SetActive(CurrentNetwork.ActiveAddresses);
            NabSearch.Instance.NabPingerEvent += Instance_NabPingerEvent;                 
            NabSearch.Instance.FindInstalledDevices();
            
        }

        private volatile int FinalCount = 0;
        private Dictionary<NabObject, NabEventArgs> ActiveDeviceStore = new Dictionary<NabObject, NabEventArgs>();
        private void Instance_NabPingerEvent(EventArgs args)
        {
            var arg = args as NabEventArgs;
            if (arg.Result)
            {
                if (!ActiveDeviceStore.ContainsKey(arg.Device))
                    ActiveDeviceStore.Add(arg.Device, arg);

                DisplayDevice(arg.Device);
            }
            FinalCount++;
            if (FinalCount >= CurrentNetwork.ActiveAddresses.Count)
            {
                SetProgress(progBarNab, false);
                if(!Program.HasData)
                {
                    Program.SetItem(lstActiveDevices, NONE_FOUND);
                }
            }
                
                          
        }

        private void DisplayDevices(List<NabObject> activeDevices)
        {
            
            if (activeDevices.Count > 0)
            {
                string[] devices = new string[activeDevices.Count];
                var count = 0;
                foreach (var nab in activeDevices)
                {
                    devices[count] = string.Format("{0} | {1}", 
                        nab.Address, nab.MerchantNumber);
                    count++;
                }
                Program.SetItems(lstActiveDevices, devices);
            }
            else
            {
                Program.SetItems(lstActiveDevices, new string[] { "None Found" });
            }
            Program.SetProgress(progBarNab, false);
            
        }

        

        private void DisplayDevice(NabObject nab)
        {
            
            
            var devText = string.Format("{0} | {1}", nab.Address, nab.MerchantNumber, fcount);
            fcount++;
            Program.SetItem(lstActiveDevices, devText);
            
            if(fcount == CurrentNetwork.ActiveAddresses.Count)
                Program.SetProgress(progBarNab, false);

        }

        #region Elavon

        private static volatile List<string> OutputList = new List<string>();
        private System.Timers.Timer elavonOutputTick;
        private static volatile int OutputCount = 0;
        private static readonly object mutex = new object();

        private void PushOutput(string msg)
        {
            lock (mutex)
            {
                NeedsUpdate = true;
                Console.WriteLine(string.Format("{0} - Count {1}",
                msg, OutputList.Count));
                OutputList.Add(msg);
            }
            
        }

        private void StartElavonOutputTick()
        {
            elavonOutputTick = new System.Timers.Timer(500);
            elavonOutputTick.Elapsed += ElavonOutputTick_Elapsed;
            elavonOutputTick.Start();
            Program.ToDispose(elavonOutputTick);
            Console.WriteLine("Tick started");
        }
        private static volatile bool NeedsUpdate = false;
        private void ElavonOutputTick_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (NeedsUpdate)
            {
                lock (mutex)
                {
                    var tmp = new List<string>(OutputList);

                    if (txtOutput.InvokeRequired)
                    {
                        txtOutput.BeginInvoke((MethodInvoker)delegate ()
                        {
                            txtOutput.Clear();
                            foreach (var item in tmp)
                            {
                                txtOutput.AppendText(item + Environment.NewLine);
                            }
                            txtOutput.Invalidate();
                        });
                    }
                    else
                    {
                        txtOutput.Clear();
                        foreach (var item in tmp)
                        {
                            txtOutput.AppendText(item + Environment.NewLine);
                        }
                        txtOutput.Invalidate();
                    }
                    NeedsUpdate = false;
                }
            }

            if(IsRunningElavonInstaller)
            {
                if(btnShowUSBSteps.Enabled || btnFindUsb.Enabled || btnPrintToLog.Enabled)
                {
                    Program.SetEnabled(btnFindUsb, false);
                    Program.SetEnabled(btnShowUSBSteps, false);
                    Program.SetEnabled(btnPrintToLog, false);
                }
                
            }
            else
            {
                Program.SetEnabled(btnFindUsb, true);
                Program.SetEnabled(btnShowUSBSteps, true);
                Program.SetEnabled(btnPrintToLog, true);
            }
        }

        private void btnShowUSBSteps_Click(object sender, EventArgs e)
        {
            ElavonInstaller.Instance.DisplayUSBHIDSteps();
            Program.SetChecked(chkUsbHid, true);
        }

        private void btnDetectDrivers_Click(object sender, EventArgs e)
        {
            if (IsRunningElavonInstaller)
                PushOutput("Installer Running Please Wait.");
            else
            {
                
                if (chkUsbHid.Checked == false)
                {
                    MessageBox.Show(null, "Devices must be USB-HID configured, please configure devices, then select the checkbox when finished."
                        , "Ingenico Install Wizard", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                IsRunningElavonInstaller = true;
                new Thread(() =>
                {
                    IsWizard = true;
                    var items = new List<ParameterizedThreadStart>();
                    OldDriverList = new List<InstalledProgram>();
                    StartElavonOutputTick();

                    PushOutput("Starting Elavon Wizard");
                    items.Add(FindInstalledDrivers);
                    items.Add(DetectUSBDevices);
                    items.Add(InstallDrivers);
                    items.Add(FindInstalledDrivers);
                    items.Add(Install_RBA_Interop);
                    items.Add(ContinueIngenicoInstall);
                    items.Add(RunDelay);
                    items.Add(DetectUSBDevices);
                    items.Add(FinalizeInstall);
                    items.Add(Cleanup);
                    threadEvent = new ManualResetEvent(false);
                    Executer(items, threadEvent);
                }).Start();
            }

            
        }

        private void Cleanup(object obj)
        {
            PushOutput("Cleaning Up");

            var curd = Directory.GetCurrentDirectory();
            var rtposd = Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.MyDocuments), RTPOS_FOLDER);

            if(curd != rtposd)
            {
                var rba = Path.Combine(Program.ExecutingDirectory,
                Program.RBA_INTEROP_FILE_NAME);
                if (File.Exists(rba))
                {
                    File.Delete(rba);
                }
                var igf = Path.Combine(Program.ExecutingDirectory, Program.INGENICO_SETUP_FILE_NAME);
                if (File.Exists(igf))
                    File.Delete(igf);
            }            
            PushOutput("Finished");
        }

        public static volatile bool CancelExecuter = false;

        private void Executer(List<ParameterizedThreadStart> items, ManualResetEvent _threadEvent)
        {
            Program.SetProgress(progbarElavon, true);
            foreach (var item in items)
            {
                _threadEvent.Reset();
                var thread = new Thread(item);
                if(!CancelExecuter)
                    thread.Start(_threadEvent);
                _threadEvent.WaitOne();                                  
            }
            if (IsWizard)
                IsWizard = false;
            Program.SetProgress(progbarElavon, false);
            _threadEvent.Dispose();
        }

        private void FinalizeInstall(object obj)
        {
            var _threadEvent = obj as ManualResetEvent;
            Program.SetProgress(progbarElavon, true);
            if (ElavonInstaller.Instance.IngenicoDeviceDetected)
            {
                MessageBox.Show(null, 
                    "Ingenico Device appears to be installed, try a test in RTPOS", 
                    "Ingenico Installer", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(null,
                    "Ingenico Device install may have failed, you may need to try manual install, or the device is not showing in USB list.",
                    "Ingenico Installer",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            _threadEvent.Set();
            Program.SetProgress(progbarElavon, false);
            IsRunningElavonInstaller = false;
        }


        private void Install_RBA_Interop(object obj)
        {
            var _threadEvent = obj as ManualResetEvent;
            Program.SetProgress(progbarElavon, true);
            PushOutput("Downloading Latest RBA_Interop.dll");
            new Thread(() =>
            {
                var dllpath = string.Empty;
                if (ElavonInstaller.Instance.DownloadRBADll(out dllpath))
                {
                    var destpath = Path.Combine(Environment.GetFolderPath(
                            Environment.SpecialFolder.MyDocuments),
                            RTPOS_FOLDER, Program.RBA_INTEROP_FILE_NAME);
                    try
                    {
                        File.Copy(dllpath, destpath, true);
                    }
                    catch(Exception e)
                    {
                        RBA_Failed(e.Message);
                    }

                    if (File.Exists(destpath))
                    {
                        PushOutput("RBA_Interop Install Complete");                        
                    }                       
                    else
                        RBA_Failed("File does not exist");
                }
                else
                {
                    RBA_Failed("Download Failed");                    
                }
                _threadEvent.Set();
                Program.SetProgress(progbarElavon, false);
            }).Start();
        }

        private void RBA_Failed(string str = "")
        {
            MessageBox.Show(null,
                string.Format("RBA_Interop.dll Install failed, please download and install manually. {0}", str),
                "RBA_Interop Install", MessageBoxButtons.OK, MessageBoxIcon.Error);
            PushOutput(string.Format("RBA_Interop Install Failed, please add manually {0}", str));
        }

        private volatile int DelayTime = 0;
        private Thread portTestThread;
        private Thread emvThread;
        private System.Timers.Timer conConnectTimer;

        private void ContinueIngenicoInstall(object obj)
        {
            var _threadEvent = obj as ManualResetEvent;
            Program.SetProgress(progbarElavon, true);
            if (IngenicoDriversDetected)
            {
                MessageBox.Show(null, "Plug in the Ingenico Machine to a new USB port, make sure it is powered on, then click Ok.", 
                    "USB Detection", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop);
                DelayTime = 60000;
            }
            else
            {
                PushOutput("Cannot Continue Install");
            }
            Program.SetProgress(progbarElavon, false);
            _threadEvent.Set();
        }

        

        private void RunDelay(object obj)
        {
            var _threadEvent = obj as ManualResetEvent;
            var seconds = DelayTime / 1000;
            PushOutput(string.Format("Allowing Device USB Association - {0} seconds", seconds));
            Program.SetProgress(progbarElavon, true);
            Task.Delay(DelayTime, Program.GetCancelToken()).Wait();
            _threadEvent.Set();
            Program.SetProgress(progbarElavon, false);
        }

        private void InstallDrivers(object obj)
        {
            var _threadEvent = obj as ManualResetEvent;
            Program.SetProgress(progbarElavon, true);
            PushOutput(Environment.NewLine);
            PushOutput("Starting Driver Install");

            new Thread(() =>
            {
                var driverPath = string.Empty;
                if (ElavonInstaller.Instance.DownloadDriver(out driverPath))
                {
                    ExecuteProcess(driverPath);
                    PushOutput("Driver Install Complete");
                    IsWizard = false;
                }
                else
                {
                    PushOutput("Driver Download Failed");
                }
                _threadEvent.Set();
                Program.SetProgress(progbarElavon, false);
            }).Start();
        }

        private void FindInstalledDrivers(object obj)
        {
            IngenicoDriversDetected = false;
            var _threadEvent = obj as ManualResetEvent;
            Program.SetProgress(progbarElavon, true);
            PushOutput(Environment.NewLine);
            PushOutput("Looking for Installed Drivers");
           

            new Thread(() =>
            {
                PushOutput("Searching Registry...Please Wait...");
                var installedDrivers = ElavonInstaller.Instance.GetInstalledIngenicoDrivers();
                while (ElavonInstaller.Instance.IsGettingDrivers)
                {
                    Thread.Sleep(50);
                }
                if(installedDrivers.Count > 0)
                {
                    IngenicoDriversDetected = true;
                    foreach (var driv in installedDrivers)
                    {
                        var item = string.Format(
                            "Caption: {0} | Desc: {1}", 
                            driv.Name, driv.InstallLocation);
                        PushOutput(item);
                    }
                    if (IsWizard)
                    {
                        var dr = MessageBox.Show(null,
                           "A previously installed driver has been found.  Would you like to uninstall it?",
                           "Ingenico Drivers Found", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (dr == DialogResult.Yes)                        
                            RemoveOldDrivers(installedDrivers);
                    }
                }
                else
                {
                    IngenicoDriversDetected = false;
                }
               
                _threadEvent.Set();
                Program.SetProgress(progbarElavon, false);
            }).Start();
            
        }
       

        private void RemoveOldDrivers(List<InstalledProgram> installedDrivers)
        {
            Console.WriteLine("Removing Old Drivers");
            MaxItemsToUninstall = installedDrivers.Count;
            UninstallCount = 0;
            foreach (var prog in installedDrivers)
            {
                if(prog.UninstallString.Length > 0 || !prog.UninstallString.Equals(string.Empty))
                {
                    
                    if (File.Exists(prog.UninstallString))
                    {   
                            if (ExecuteProcess(prog.UninstallString))  
                                UninstallCount++;
                            else                        
                                PushOutput(string.Format("Failed to Execute Process: {0}",
                                    prog.UninstallString));
                    }
                    else
                    {
                        PushOutput(string.Format("Process does not exist: {0}", prog.UninstallString));
                        PushOutput("Trying default location.");
                        var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), RTPOS_FOLDER, ELAVON_UNINSTALL_EXE);
                        if (ExecuteProcess(path))
                            UninstallCount++;
                        else
                            PushOutput(string.Format("Failed to Execute Process: {0}",
                                prog.UninstallString));
                    }
                }
                if(UninstallCount != MaxItemsToUninstall)
                {
                    MessageBox.Show(null,
                               "Program was not able to uninstall all drivers, uninstall manually.",
                               "Ingenico Driver Uninstaller", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private bool ExecuteProcess(string path)
        {

            try
            {
                Process uninstaller = new Process();
                uninstaller.StartInfo.FileName = path;
                uninstaller.StartInfo.RedirectStandardInput = true;
                uninstaller.StartInfo.RedirectStandardOutput = true;
                uninstaller.StartInfo.RedirectStandardError = true;
                uninstaller.StartInfo.UseShellExecute = false;
                uninstaller.OutputDataReceived += (sender, args) =>
                {
                    Console.WriteLine(args.Data);
                };
                uninstaller.ErrorDataReceived += (sender, args) =>
                {
                    Console.WriteLine(args.Data);
                };
                uninstaller.Start();
                uninstaller.BeginOutputReadLine();
                uninstaller.BeginErrorReadLine();
                uninstaller.WaitForExit();
            }
            catch
            {
                return false;
            }

            return true;
            
        }

        private void DetectUSBDevices(object obj)
        {
            var _threadEvent = obj as ManualResetEvent;
            
            new Thread(() =>
            {
                DetectUSBDevicesEx();
                _threadEvent.Set();
            }).Start();
            
                   
            
        }
        #endregion

        private void DetectUSBDevicesEx()
        {
            Program.SetProgress(progbarElavon, true);
            PushOutput(Environment.NewLine);
            PushOutput("Listing Currently Installed USB Devices");
            PushOutput(Environment.NewLine);
           
            var usbDevices = ElavonInstaller.Instance.GetUSBDevices(IsWizard);
            while (ElavonInstaller.Instance.IsGettingUSBDevices)
            {
                //This is probbaly not needed
                Thread.Sleep(50);
            }
            if (usbDevices.Count > 0)
            {
                foreach (var usb in usbDevices)
                {
                    string susb = string.Format("Name: {0} - {1} | Status: {2} | DeviceID: {3}",
                        usb.Name,
                        usb.Description,
                        usb.Status,
                        usb.DeviceID
                        );
                    PushOutput(susb);
                }
                PushOutput(string.Format("Found {0} installed devices", usbDevices.Count));
                PushOutput(Environment.NewLine);
            }
            else
            {
                PushOutput("WMI Failed - Skipping");
            }
            Program.SetProgress(progbarElavon, false);
           
         }

        private void btnFindUsb_Click(object sender, EventArgs e)
        {


            new Thread(() =>
            {
                DetectUSBDevicesEx();                
            }).Start();

        }

        private void btnPrintToLog_Click(object sender, EventArgs e)
        {
            if(txtOutput.Lines.Length > 0)
            {
                var logpath = Path.Combine(Program.ExecutingDirectory, "Elavon_Install_Log.txt");
                File.AppendAllLines(logpath, txtOutput.Lines);
                Process.Start(logpath);

            }
        }

        private void lstOpen_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var selIndex = this.lstOpen.IndexFromPoint(e.Location);
            if (selIndex == ListBox.NoMatches)
                return;
            
            string item = string.Format("{0}", 
                lstOpen.Items[selIndex]
                );
            string[] items = { item };
            new frmDisplay(items).ShowDialog();
            

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Shutdown();
        }

        private void nABEPXToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var filepath = Path.Combine(
                    Program.ExecutingDirectory,
                    "Nab_Install_Log.txt");

            if (File.Exists(filepath))
                File.Delete(filepath);


            StringBuilder printer = new StringBuilder();

            printer.Append("Network Information");
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            printer.AppendFormat("Gateway: {0}",
                CurrentNetwork.GatewayAddress.ToString());
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            printer.AppendFormat("Subnet Mask: {0}", 
                CurrentNetwork.ValidSubnets.First().ToString());
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            printer.AppendFormat("DNS1: {0}", 
                CurrentNetwork.DnsAddresses[0]);
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            printer.AppendFormat("DNS2: {0}", 
                CurrentNetwork.DnsAddresses[1]);
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            printer.AppendFormat("# of Active IP: {0}", 
                CurrentNetwork.ActiveAddresses.Count());
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            printer.Append("Open IP Addressess");
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            foreach (var addr in CurrentNetwork.FreeAddresses)
            {
                printer.AppendFormat("{0}", addr + Environment.NewLine);
            }

            printer.Append(Environment.NewLine);

            printer.Append("Active IP Addressess");
            printer.Append(Environment.NewLine);
            printer.Append(Environment.NewLine);

            foreach (var aaddr in CurrentNetwork.ActiveAddresses)
            {
                printer.AppendFormat("{0}", aaddr + Environment.NewLine);
            }

            printer.Append(Environment.NewLine);

            printer.Append("Found Devices");

            printer.Append(Environment.NewLine);

            if (lstActiveDevices.Items.Count > 0)
            {
                foreach(var devaddr in lstActiveDevices.Items)
                {
                    printer.AppendFormat("{0}", devaddr + Environment.NewLine);
                }
            }
            else
            {
                printer.Append("None Found");
            }

            

            File.AppendAllText(filepath, printer.ToString());

            Process.Start(filepath);
            

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://myrtpos.net/download/Help-Docs/Readme.pdf");
        }

        private void elavonOutputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtOutput.Lines.Length > 0)
            {
                File.AppendAllLines(Path.Combine(Program.ExecutingDirectory, "Elavon_Install_Log.txt"), txtOutput.Lines);
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Shutdown();
        }

        private void btnEMVInstall_Click(object sender, EventArgs e)
        {

            EnableElavonControls(false);

            StartElavonOutputTick();

            

            var dr = MessageBox.Show(null,
                "This will install EMV/Converage for Elavon, this is not for all stores, only use if directed to do so. Are you sure?",
                "EMV ONLY", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                var posProcs = Process.GetProcessesByName("RealtimePOS");

                if (posProcs.Length > 0)
                {
                    PushOutput("Closing all RTPOS Instances");
                    foreach (var pos in posProcs)
                        pos.Kill();
                }

                emvThread = new Thread(new ThreadStart(InstallEMV));
                emvThread.Start();
                PushOutput("Running EMV Install");
            }
               

        }

        private void EnableElavonControls(bool v)
        {
            Program.SetEnabled(btnDetectDrivers, v);
            Program.SetEnabled(btnShowUSBSteps, v);
            Program.SetEnabled(btnFindUsb, v);
            Program.SetEnabled(chkUsbHid, v);
            Program.SetEnabled(btnEMVInstall, v);
        }

        private void InstallEMV()
        {
            
            Program.SetProgress(progbarElavon, true);

            var rtemvPath = string.Empty;
            PushOutput("Downloading RT Converge Files");
            if(ElavonInstaller.Instance.DownloadRtEMVFiles(out rtemvPath))
            {
                PushOutput(string.Format("Downloaded RTConverage to {0}", rtemvPath));
                PushOutput("Downloading ConvergeConnect");
                var ccpath = string.Empty;
                if(ElavonInstaller.Instance.DownloadConverageConnect(out ccpath))
                {
                    PushOutput(string.Format("Downloaded ConvergeConnect to {0}", ccpath));
                    PushOutput("Downloads Complete, Installing RT Converage Files.");
                    if (ExecuteProcess(rtemvPath))
                    {
                        PushOutput("RT Converage Install Completed");
                        PushOutput("Installing ConvergeConnect");
                        if (ExecuteProcess(ccpath))
                        {
                            PushOutput("Converge Connect installed successfully");
                            
                            var execPath = string.Empty;
                            if (Environment.Is64BitOperatingSystem)
                                execPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), ELAVON, CONVERAGE_CONNECT, C_C_ADMIN_EXE );
                            else
                                execPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), ELAVON, CONVERAGE_CONNECT, C_C_ADMIN_EXE );
                            PushOutput(string.Format("Running Converge Connect at {0}", execPath));
                            if (File.Exists(execPath))
                            {
                                Task.Delay(1500).Wait();

                                ProcessStartInfo psi = new ProcessStartInfo()
                                {
                                    UseShellExecute = true,
                                    FileName = execPath,

                                };
                                Process.Start(psi);
                                PushOutput("EMV Install Complete");
                            }
                            else
                            {
                                PushOutput("EMV Install may have failed, please try manually.");
                            }

                                                       
                        }
                        else
                        {
                            PushOutput("Could not start ConvergeConnect, or the install failed");
                        }
                    }
                    else
                    {
                        PushOutput("Could not start RTConverage Installer, or the install failed");
                    }
                }
                else
                {
                    PushOutput(string.Format("Failed to download Converage Connect, you will need to install manually, The path for the RT converge files is {0}", rtemvPath));
                }
            }
            else
            {
                PushOutput("Failed to download RT Converge Files, please install manually");
            }

            Program.SetProgress(progbarElavon, false);
            EnableElavonControls(true);
        }
    }
}
