﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RTCCInstaller
{
    public class NetworkInfoObject
    {
        public IPAddress GatewayAddress;
        public List<IPAddress> FreeAddresses;
        public List<IPAddress> ActiveAddresses;
        public List<IPAddress> DnsAddresses;
        public List<IPAddress> ValidSubnets;

        

        public NetworkInfoObject()
        {
            ResetLists();
        }

        private void ResetLists()
        {
            
            GatewayAddress = IPAddress.Loopback;
            FreeAddresses = new List<IPAddress>();
            ActiveAddresses = new List<IPAddress>();
            DnsAddresses = new List<IPAddress>();
            ValidSubnets = new List<IPAddress>();
        }


    }
}
