﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTCCInstaller
{
    public class InstalledProgram
    {
        public string Caption { get; set; }
        public string Description { get; set; }
        public string InstallDate { get; set; }
        public string InstallLocation { get; set; }
        public string Name { get; set; }
        public string Publisher { get; internal set; }
        public string Vendor { get; set; }
        public string Version { get; set; }
        public string UninstallString { get; set; }
    }
}
