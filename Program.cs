﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTCCInstaller
{
    static class Program
    {
        public static List<Thread> ThreadList = new List<Thread>();
        public static volatile bool IsNabSearchUsed = false;
        public static string ExecutingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        internal volatile static bool HasData = false;
        private static CancellationTokenSource tSource = new CancellationTokenSource();

        public static List<IDisposable> DisposeList = new List<IDisposable>();
        internal const string RBA_INTEROP_FILE_NAME = "RBA_Interop.dll";
        internal const string INGENICO_SETUP_FILE_NAME = "Setup_Ingenico.exe";

        [STAThread]
        static void Main()
        {


            if (!IsAdministrator())
            {
                MessageBox.Show(null, 
                   "RT CC Quick Install should be ran as admin, some features may not work properly", 
                   "Run As Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
            }



            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        internal static bool IsAdministrator()
        {
            return (new WindowsPrincipal(WindowsIdentity.GetCurrent())).IsInRole(WindowsBuiltInRole.Administrator);
        }


       

        public static void Shutdown()
        {
            try
            {
                tSource.Cancel();

                if (ThreadList.Count > 0)
                    foreach (var t in ThreadList)
                        if (t.ThreadState != System.Threading.ThreadState.Aborted)
                            t.Abort();

                if (IsNabSearchUsed)
                    NabSearch.Instance.Dispose();

                foreach (var item in DisposeList)
                    item.Dispose();

                Application.Exit();
                Environment.Exit(0);
            }
            catch { }
           
            

        }

        public static void ToDispose(IDisposable item)
        {
            DisposeList.Add(item);
        }

        public static void ToDispose(Thread thread)
        {
            ThreadList.Add(thread);
        }

        

        internal static void SetVisible(Control control, bool visible)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.Visible = visible;
                    control.Invalidate();
                });
            }
            else
            {
                control.Visible = visible;
                control.Invalidate();
            }
        }

        internal static void SetText(Control control, string text)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.Text = text;
                    control.Invalidate();
                });
            }
            else
            {
                control.Text = text;
                control.Invalidate();
            }
        }

        internal static void SetProgress(ProgressBar prog, bool start)
        {
            

            if (prog.InvokeRequired)
            {
                prog.BeginInvoke((MethodInvoker)delegate ()
                {                    
                    prog.Style = start ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
                    prog.Invalidate();
                });
            }
            else
            {
                prog.Style = start ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
                prog.Invalidate();
            }

            
        }

        internal static void SetForground(Control control, Color color)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.ForeColor = color;
                    control.Invalidate();
                });
            }
            else
            {
                control.ForeColor = color;
                control.Invalidate();
            }
        }

        internal static void SetEnabled(Control control, bool enabled)
        {


            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.Enabled = enabled;
                    control.Invalidate();
                });
            }
            else
            {
                control.Enabled = enabled;
                control.Invalidate();
            }


        }

        internal static void SetItems(ListBox lst, string[] v)
        {

            if (lst.InvokeRequired)
            {
                lst.BeginInvoke((MethodInvoker)delegate ()
                {
                    lst.DataSource = null;
                    lst.DataSource = v;
                    lst.Invalidate();
                });
            } else
            {
                lst.DataSource = null;
                lst.DataSource = v;
                lst.Invalidate();
            }
        }

        internal static void SetItem(ListBox lst, string v)
        {
            if(lst != null || v != null || v != string.Empty)
            {
                Program.HasData = true;
                if (lst.InvokeRequired)
                {
                    lst.BeginInvoke((MethodInvoker)delegate ()
                    {
                        lst.Items.Add(v);
                        lst.Invalidate();
                    });
                }
                else
                {
                    lst.Items.Add(v);
                    lst.Invalidate();
                }
            }
            
        }

        internal static void ClearItems(ListBox lst)
        {

            if (lst.InvokeRequired)
            {
                lst.BeginInvoke((MethodInvoker)delegate ()
                {
                    lst.DataSource = null;
                    lst.Invalidate();
                });
            }
            else
            {
                lst.DataSource = null;
                lst.Invalidate();
            }
        }

        internal static void SetChecked(CheckBox control, bool v)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.Checked = v;
                    control.Invalidate();
                });
            }
            else
            {
                control.Checked = v;
                control.Invalidate();
            }
        }

        internal static void SetEnabled(ToolStripMenuItem mitem, bool v)
        {
            if (mitem.GetCurrentParent().InvokeRequired)
            {
                mitem.GetCurrentParent().BeginInvoke((MethodInvoker)delegate ()
                {
                    mitem.Enabled = v;
                    mitem.GetCurrentParent().Invalidate();
                });
            }
            else
            {
                mitem.Enabled = v;
                mitem.GetCurrentParent().Invalidate();
            }
        }
        
        internal static CancellationToken GetCancelToken()
        {
            return tSource.Token;
        }
    }
}
