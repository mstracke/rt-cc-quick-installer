﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTCCInstaller
{
    public class ElavonInstaller
    {

        private static ElavonInstaller _this;
        private string ElavonConfigFile = Path.Combine(Program.ExecutingDirectory, "ElavonConfigSteps.txt");
        const string DEFAULT_UNINSTALL_STRING = @"C:\Users\Admin\Documents\RealtimePOS\unins000.exe";
        const string ELAVON_INSTALL_STRING = @"https://myrtpos.net/download/elavon/Setup_Ingenico.exe";
        const string ELAVON_RBA_INTEROP_STRING = @"https://myrtpos.net/download/elavon/RBA_Interop.dll";


        public volatile bool IsGettingUSBDevices = false;
        public volatile bool IsGettingDrivers = false;

        public void DisplayUSBHIDSteps()
        {
            File.WriteAllText(ElavonConfigFile, InstallStepsString);
            Process.Start(ElavonConfigFile);
        }
        
        internal List<InstalledProgram> GetInstalledIngenicoDriversEx()
        {
            var ret = new List<InstalledProgram>();
            ret = GetInstalledProgramsWMI();            
            return ret;
        }

        internal List<InstalledProgram> GetInstalledIngenicoDrivers()
        {
            var ret = new List<InstalledProgram>();
            ret = GetInstalledProgramsReg();
            return ret;
        }

        private List<InstalledProgram> GetInstalledProgramsReg()
        {
            var ret = new List<InstalledProgram>();
            string uninstallKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(uninstallKey))
            {
                foreach (string skName in rk.GetSubKeyNames())
                {
                    using (RegistryKey sk = rk.OpenSubKey(skName))
                    {
                        try
                        {
                            var bMove = false;
                            foreach (var it in nameSearch)
                            {
                                if (sk.GetValue("InstallLocation").
                                    ToString().Contains(it) ||
                                    sk.GetValue("DisplayName").
                                    ToString().Contains(it) ||
                                    sk.GetValue("Publisher").
                                    ToString().Contains(it))
                                {
                                    bMove = true;
                                }
                            }
                            if (sk.GetValue("Publisher").
                                    ToString().Contains(companySearch))
                            {
                                bMove = true;
                            }


                            if (!bMove)
                                continue;



                            var item = new InstalledProgram();
                            item.Name = sk.GetValue("DisplayName").ToString();
                            item.Version = sk.GetValue("DisplayVersion").ToString();
                            item.InstallDate = sk.GetValue("InstallDate").ToString();
                            item.Publisher = sk.GetValue("Publisher").ToString();
                            item.InstallLocation = sk.GetValue("InstallLocation").ToString();
                            item.UninstallString = sk.GetValue("UninstallString").ToString();
                            ret.Add(item);
                        }
                        catch
                        { }
                    }
                }
            }
            return ret;
        }
        private readonly string[] nameSearch = { "RTPOS Ingenico iSC250 version 1.0", "Ingenico USB Drivers Package" };
        const string companySearch = "EzLogic Data Systems, Inc.";
        private List<InstalledProgram> GetInstalledProgramsWMI()
        {
            IsGettingDrivers = true;
            var installedDrivers = new List<InstalledProgram>();
            ManagementObjectCollection collection;
            using (var searcher = new ManagementObjectSearcher(@"SELECT * From Win32_Product"))
                collection = searcher.Get();

            foreach(var driv in collection)
            {
                if (driv == null)
                    continue;

                var bMove = false;
                foreach(var it in nameSearch)
                {
                    if (driv.GetPropertyValue("Caption").
                        ToString().Contains(it) ||
                        driv.GetPropertyValue("Description").
                        ToString().Contains(it) ||
                        driv.GetPropertyValue("Name").
                        ToString().Contains(it) ||
                        driv.GetPropertyValue("Vendor").
                        ToString().Contains(it))
                    {
                        bMove = true;
                    }
                }
                if(driv.GetPropertyValue("Description").
                        ToString().Contains(companySearch) ||                        
                        driv.GetPropertyValue("Vendor").
                        ToString().Contains(companySearch))
                {
                    bMove = true;
                }
                

                if (!bMove)
                    continue;

                var idriver = new InstalledProgram();
                idriver.Caption = (string)driv.GetPropertyValue("Caption");
                idriver.Description = (string)driv.GetPropertyValue("Description");
                idriver.InstallDate = (string)driv.GetPropertyValue("InstallDate");
                idriver.InstallLocation = (string)driv.GetPropertyValue("InstallLocation");
                idriver.Name = (string)driv.GetPropertyValue("Name");
                idriver.Vendor = (string)driv.GetPropertyValue("Vendor");
                idriver.Version = (string)driv.GetPropertyValue("Version");
                idriver.UninstallString = DEFAULT_UNINSTALL_STRING;

                installedDrivers.Add(idriver);
            }
            collection.Dispose();
            IsGettingDrivers = false;
            return installedDrivers;
        }

        internal List<USBDeviceInfo> GetUSBDevices(bool IsWizard)
        {
            var ret = new List<USBDeviceInfo>();
            try
            {
                ret = GetUSBDevicesEx(IsWizard);
            }
            catch
            { }
            
            return ret;    
        }
        

        private List<USBDeviceInfo> GetUSBDevicesEx(bool IsWizard)
        {
            
            IsGettingUSBDevices = true;
            IngenicoDeviceDetected = false;
            var devices = new List<USBDeviceInfo>();

            ManagementObjectCollection collection;
            using (var searcher = new ManagementObjectSearcher(@"SELECT * From Win32_USBHub"))
                collection = searcher.Get();

            foreach(var dev in collection)
            {
                if (dev == null)
                    continue;


                USBDeviceInfo usbdev = new USBDeviceInfo();
                
                usbdev.Caption = dev.GetPropertyValue("Caption").ToString();
                usbdev.Description = dev.GetPropertyValue("Description").ToString();                
                usbdev.PNPDeviceID = dev.GetPropertyValue("PNPDeviceID").ToString();
                usbdev.Status = dev.GetPropertyValue("Status").ToString();                
                usbdev.SystemCreationClassName = 
                    dev.GetPropertyValue("SystemCreationClassName").ToString();                
                usbdev.DeviceID = dev.GetPropertyValue("DeviceID").ToString();
                usbdev.Name = dev.GetPropertyValue("Name").ToString();
                devices.Add(usbdev);
                
                
                if (usbdev.Caption.ToLower().Contains("ingenico") ||
                usbdev.Caption.ToLower().Contains("elavon") ||
                usbdev.Caption.ToLower().Contains("ics250") ||
                usbdev.Name.ToLower().Contains("ingenico") ||
                usbdev.Name.ToLower().Contains("elavon") ||
                usbdev.Name.ToLower().Contains("ics250"))
                {
                    IngenicoDeviceDetected = true;
                    if (IsWizard)
                    {
                        var msg = string.Format("Ingenico Device detected:  {0}{1}{2}{1}{3}",
                                    usbdev.Name, Environment.NewLine,
                                    "If this is an install/reinstall unplug the device now.",
                                    "Failing to remove device before install may lead to install failing");
                        MessageBox.Show(null, msg, "Install May Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                }
                

                
            }
            collection.Dispose();
            IsGettingUSBDevices = false;
            return devices;
        }

        public static ElavonInstaller Instance
        {
            get
            {
                if (_this == null)
                    _this = new ElavonInstaller();

                return _this;
            }
        }

        public bool IngenicoDeviceDetected { get; private set; }

        private readonly string InstallStepsString = "1. Hold down [Clear] and [-] the device should reboot" + Environment.NewLine +
            "2. Wait until it gets to the Retail Base or blue \"RBA\" screen then enter: 2 6 3 4 [Enter]" + Environment.NewLine +
            "3. It should display \"Press + for menu\"" + Environment.NewLine +
            "4. Hit + until the TDA option is highlighted." + Environment.NewLine +
            "5. Hit[Enter]" + Environment.NewLine +
            "6. On 0-Configuration hit[Enter]" + Environment.NewLine +
            "7. On 0-Communication hit[Enter]" + Environment.NewLine +
            "8. On 0-Select Comm.Type hit[Enter]" + Environment.NewLine +
            "9. Hit the[-] key to move down to 2-USB-HID (if you pass it, hit the[+] key)" + Environment.NewLine +
            "10. Hit[Enter]" + Environment.NewLine +
            "11. Hit[Cancel] to move back to the Communication page" + Environment.NewLine +
            "12 .Hit[Cancel] to move back to the Configuration page" + Environment.NewLine +
            "13. It will prompt: Save and Reboot." + Environment.NewLine +
            "14. Hit[-] to select 1 –Yes." + Environment.NewLine +
            "15. Hit[Enter]" + Environment.NewLine +
            "16. At that point it should reboot" + Environment.NewLine;

        internal bool DownloadDriver(out string driverPath)
        {
            driverPath = Path.Combine(Program.ExecutingDirectory, Program.INGENICO_SETUP_FILE_NAME);
            using (var wc = new WebClient())
            {
                wc.DownloadFile(ELAVON_INSTALL_STRING, 
                    driverPath);

            }
            if (File.Exists(driverPath))
                return true;
            else
                return false;
        }
        const string converageName = "ConvergeConnectSetup.exe";
        const string rtconverageName = "rtconverge.exe";
        const string converageConnectPath = "https://myrtpos.net/download/elavon/ConvergeConnectSetup.exe";
        const string rtConverage = "http://rtpos.com/rtconverge.exe";

        internal bool DownloadRtEMVFiles(out string path)
        {
            path = Path.Combine(Program.ExecutingDirectory, rtconverageName);
            using(var wc = new WebClient())
            {
                wc.DownloadFile(rtConverage, path);
            }
            if (File.Exists(path))
                return true;
            else
                return false;
        }

        internal bool DownloadRBADll(out string dllpath)
        {
            dllpath = Path.Combine(Program.ExecutingDirectory, Program.RBA_INTEROP_FILE_NAME);
            using(var wc = new WebClient())
            {
                wc.DownloadFile(ELAVON_RBA_INTEROP_STRING, dllpath);
            }
            if (File.Exists(dllpath))
                return true;
            else
                return false;
        }

        internal bool DownloadConverageConnect(out string path)
        {
            path = Path.Combine(Program.ExecutingDirectory, converageName);
            using (var wc = new WebClient())
            {
                wc.DownloadFile(converageConnectPath, path);
            }
            if (File.Exists(path))
                return true;
            else
                return false;
        }

    }

   
}
