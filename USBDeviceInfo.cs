﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTCCInstaller
{
    internal class USBDeviceInfo
    {
        
        internal string Caption { get; set; }
        
       
        internal string CreationClassName { get; set; }
        internal byte CurrentAlternateSettings { get; set; }
        internal byte CurrentConfigValue { get; set; }
        internal string Description { get; set; }   
        internal string PNPDeviceID { get; set; }
        internal string Status { get; set; }
        internal UInt16 StatusInfo { get; set; }
        internal string SystemCreationClassName { get; set; }
        internal string DeviceID { get; set; }
        internal string Name { get; set; }
    }
}
