﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

/*
     * 
     * <RESPONSE>
     *      <LOCAL_DATE>DATE</LOCAL_DATE>
     *      <LOCAL_TIME>TIME</LOCAL_TIME>
     *      <SI_SIGNATURE_REQUIRED>N</SI_SIGNATURE_REQUIRED>
     *      <CUST_NBR>9001</CUST_NBR>
     *      <MERCH_NBR>900720</MERCH_NBR>
     *      <DBA_NBR>1</DBA_NBR>
     *      <TERMINAL_NBR>1</TERMINAL_NBR>
     *      <AUTH_RESP>S0</AUTH_RESP>
     *      <AUTH_RESP_TEXT>SI_ERROR</AUTH_RESP_TEXT>
     * </RESPONSE>
     */

namespace RTCCInstaller
{
    //DROP INSTANCE CALL AS A CLASS

    internal class NabSearch : IDisposable
    {
        const string TestUrl = "http://127.0.0.1/nabtest/fakenab/epx.php";
        const string DataString = "<DETAIL></DETAIL>";
        const int Port = 6200;
        const string Http = "http://";
        const string AppXmlContentType = "application/xml";
        private List<IPAddress> ActiveAddresses;
        private List<NabObject> ActiveDevices;
        private List<WebClient> WebClientList;

       

        private static NabSearch _this;
        
        private List<Thread> nabThreads;
        
        private volatile int MaxPings;
        private volatile int PingCount = 0;

        public delegate void NabPingerCompleteEventHandler(EventArgs args);
        public event NabPingerCompleteEventHandler NabPingerEvent;

        public static readonly object adMutex = new object();
        public static readonly object eventMutex = new object();

        public void SetActive(List<IPAddress> activeAddresses)
        {
            this.ActiveAddresses = activeAddresses;
            this.MaxPings = activeAddresses.Count;
            this.ActiveDevices = new List<NabObject>();
            WebClientList = new List<WebClient>();


            Program.IsNabSearchUsed = true;
        }


        public void Reset()
        {
            _this = null;
        }
        
        public void FindInstalledDevices()
        {
            
            nabThreads = new List<Thread>();
            var max = ActiveAddresses.Count;
            var count = -1;
            foreach(var addr in ActiveAddresses)
            {               
                if(count < max)
                {
                    count++;
                    var url = new Uri(string.Format("{0}{1}:{2}", Http, addr.ToString(), Port));

#if DEBUG
            
            
                    Console.WriteLine(string.Format("{1} - Trying Test URL As {0}", url, dbgcnt++));
                    url = new Uri(TestUrl); 
                    Console.WriteLine("Trying " + url);           
#endif
                    nabThreads.Add(new Thread(() => NabPinger(addr, url, count)));
                    Program.ToDispose(nabThreads[count]);                    
                    nabThreads[count].Start();                    
                } 
                
            }
        }

        public List<NabObject> GetActiveDevices()
        {
            return ActiveDevices;
        }

        private void NabPinger(IPAddress address, Uri url, int i)
        {
            lock (adMutex)
            {
                var nabobj = new NabObject();
                if (PingNab(address, url, out nabobj))
                {

                    ActiveDevices.Add(nabobj);

                }
            }
            
        }

        private void OnNabPingerComplete(NabEventArgs args)
        {
            lock (eventMutex)
            {
               NabPingerEvent?.Invoke(args);
               
            }            
        }

        private static volatile int dbgcnt = 0;

        private bool PingNab(IPAddress address, Uri url, out NabObject nab)
        {


            try
            {
                //Console.WriteLine("IP " + address.ToString());
                nab = new NabObject();
                var res = new NabEventArgs();
                var wc = GetWebClient();


                var result = wc.UploadString(url, DataString);


                if (result.Length > 0)
                {
                    try
                    {

                        var xmldoc = new XmlDocument();
                        xmldoc.LoadXml(result);

                        var path = "RESPONSE";
                        var nodes = xmldoc.SelectNodes(path);
                        if (nodes.Count == 1)
                        {

                            var node = nodes[0];
                            nab.Address = address;

                            nab.Date = node.SelectSingleNode("LOCAL_DATE").InnerText;
                            nab.Time = node.SelectSingleNode("LOCAL_TIME").InnerText;
                            nab.CustomerNumber = node.SelectSingleNode("CUST_NBR").InnerText;
                            nab.MerchantNumber = node.SelectSingleNode("MERCH_NBR").InnerText;
                            nab.DBANumber = node.SelectSingleNode("DBA_NBR").InnerText;
                            nab.TerminalNumber = node.SelectSingleNode("TERMINAL_NBR").InnerText;
                            nab.Response = node.SelectSingleNode("AUTH_RESP").InnerText;
                            nab.ResponseText = node.SelectSingleNode("AUTH_RESP_TEXT").InnerText;

                            if (nab.Response.Equals("S0") || nab.ResponseText.Equals("SI_ERROR"))
                            {
                                nab.Color = Color.Green;
                                res.Result = true;
                                res.ResultText = "Probable Active NAB Device";
                                res.Device = nab;
                                OnNabPingerComplete(res);
                                return true;
                            }
                            else
                            {
                                nab.Color = Color.Orange;
                                res.Result = true;
                                res.ResultText = "Possible Invalid Device Setup, Unexpected responce/error, or not a NAB/EPX device.";
                                res.Device = nab;
                                OnNabPingerComplete(res);
                                return true;
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        res.Result = false;
                        res.ResultText = string.Format("Possible this is Not a NAB/EPX Device - XML Error:{1}{1}{0}", e.Message, Environment.NewLine);

                        OnNabPingerComplete(res);
                        return false;
                    }

                }
                else
                {
                    Console.WriteLine("0 result");
                }

                res.Result = false;
                res.ResultText = "Not a NAB/EPX Device";
                OnNabPingerComplete(res);
                return false;
            }
            catch (Exception e)
            {

                var res = new NabEventArgs();
                nab = new NabObject();
                res.Result = false;
                res.ResultText = string.Format("Not a NAB/EPX Device - XML Error:{2}-{1}{1}{0}", e.Message, Environment.NewLine, address);
                OnNabPingerComplete(res);
                //MessageBox.Show(e.Message);
                return false;
            }
        }

        private WebClient GetWebClient()
        {
            var webclient = new WebClient();
            webclient.Headers[HttpRequestHeader.ContentType] = AppXmlContentType;
            WebClientList.Add(webclient);
            return webclient;
        }

        public void Dispose()
        {
            foreach (var wc in WebClientList)
                wc.Dispose();
        }

        public static NabSearch Instance
        {
            get
            {
                if (_this == null)
                    _this = new NabSearch();

                return _this;
            }
        }

        
    }

    internal class NabObject
    {
        internal string MerchantNumber;
        internal string DBANumber;
        internal string TerminalNumber;
        internal IPAddress Address;
        internal string Date;
        internal string Time;
        internal string CustomerNumber;
        internal string Response;
        internal string ResponseText;
        internal Color Color;
        internal NabEventArgs _EventArgs;

        internal NabObject()
        { }

        internal NabObject(IPAddress address)
        {
            this.Address = address;
        }

        internal NabObject(IPAddress addr, string mn, string dn, string tn)
        {
            this.MerchantNumber = mn;
            this.DBANumber = dn;
            this.TerminalNumber = tn;
            this.Address = addr;
        }
    }

    internal class NabEventArgs : EventArgs
    {
        internal bool Result { get; set; }
        internal string ResultText { get; set; }
        internal NabObject Device { get; set; }
    }
}
