﻿namespace RTCCInstaller
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.MainTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblGatewayLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblGateway = new System.Windows.Forms.Label();
            this.lblSubnet = new System.Windows.Forms.Label();
            this.lblDns1 = new System.Windows.Forms.Label();
            this.lblDns2 = new System.Windows.Forms.Label();
            this.lblActiveIPCount = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNabUpdate = new System.Windows.Forms.Label();
            this.lblNabCom = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblPOSLocal = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGetNetwork = new System.Windows.Forms.Button();
            this.btnNabSearch = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lstActiveDevices = new System.Windows.Forms.ListBox();
            this.lstOpen = new System.Windows.Forms.ListBox();
            this.lstActive = new System.Windows.Forms.ListBox();
            this.progBarNab = new System.Windows.Forms.ProgressBar();
            this.lblNote = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDetectDrivers = new System.Windows.Forms.Button();
            this.btnShowUSBSteps = new System.Windows.Forms.Button();
            this.btnPrintToLog = new System.Windows.Forms.Button();
            this.btnFindUsb = new System.Windows.Forms.Button();
            this.progbarElavon = new System.Windows.Forms.ProgressBar();
            this.chkUsbHid = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btnEMVInstall = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.lblConverageStatus = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nABEPXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elavonOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTab
            // 
            this.MainTab.Controls.Add(this.tabPage1);
            this.MainTab.Controls.Add(this.tabPage2);
            this.MainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTab.Location = new System.Drawing.Point(0, 24);
            this.MainTab.Name = "MainTab";
            this.MainTab.SelectedIndex = 0;
            this.MainTab.Size = new System.Drawing.Size(407, 579);
            this.MainTab.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(399, 553);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "NAB/EPX Installer";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.progBarNab, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblNote, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.71944F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.28056F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(393, 547);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.90323F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.09677F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(393, 278);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(387, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Network Information";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lblGatewayLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblGateway, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblSubnet, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblDns1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblDns2, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblActiveIPCount, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 1, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(387, 237);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // lblGatewayLabel
            // 
            this.lblGatewayLabel.AutoSize = true;
            this.lblGatewayLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGatewayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGatewayLabel.Location = new System.Drawing.Point(3, 0);
            this.lblGatewayLabel.Name = "lblGatewayLabel";
            this.lblGatewayLabel.Size = new System.Drawing.Size(187, 39);
            this.lblGatewayLabel.TabIndex = 0;
            this.lblGatewayLabel.Text = "Gateway";
            this.lblGatewayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(187, 39);
            this.label3.TabIndex = 1;
            this.label3.Text = "Subnet Mask";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 39);
            this.label4.TabIndex = 2;
            this.label4.Text = "DNS1";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(187, 39);
            this.label5.TabIndex = 3;
            this.label5.Text = "DNS2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 39);
            this.label6.TabIndex = 4;
            this.label6.Text = "# of Active IPs";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGateway
            // 
            this.lblGateway.AutoSize = true;
            this.lblGateway.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGateway.Location = new System.Drawing.Point(196, 0);
            this.lblGateway.Name = "lblGateway";
            this.lblGateway.Size = new System.Drawing.Size(188, 39);
            this.lblGateway.TabIndex = 5;
            this.lblGateway.Text = "127.0.0.1";
            this.lblGateway.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSubnet
            // 
            this.lblSubnet.AutoSize = true;
            this.lblSubnet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSubnet.Location = new System.Drawing.Point(196, 39);
            this.lblSubnet.Name = "lblSubnet";
            this.lblSubnet.Size = new System.Drawing.Size(188, 39);
            this.lblSubnet.TabIndex = 6;
            this.lblSubnet.Text = "255.255.255.0";
            this.lblSubnet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDns1
            // 
            this.lblDns1.AutoSize = true;
            this.lblDns1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDns1.Location = new System.Drawing.Point(196, 78);
            this.lblDns1.Name = "lblDns1";
            this.lblDns1.Size = new System.Drawing.Size(188, 39);
            this.lblDns1.TabIndex = 7;
            this.lblDns1.Text = "127.0.0.1";
            this.lblDns1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDns2
            // 
            this.lblDns2.AutoSize = true;
            this.lblDns2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDns2.Location = new System.Drawing.Point(196, 117);
            this.lblDns2.Name = "lblDns2";
            this.lblDns2.Size = new System.Drawing.Size(188, 39);
            this.lblDns2.TabIndex = 8;
            this.lblDns2.Text = "127.0.0.1";
            this.lblDns2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblActiveIPCount
            // 
            this.lblActiveIPCount.AutoSize = true;
            this.lblActiveIPCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblActiveIPCount.Location = new System.Drawing.Point(196, 156);
            this.lblActiveIPCount.Name = "lblActiveIPCount";
            this.lblActiveIPCount.Size = new System.Drawing.Size(188, 39);
            this.lblActiveIPCount.TabIndex = 9;
            this.lblActiveIPCount.Text = "0";
            this.lblActiveIPCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 195);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 42);
            this.label2.TabIndex = 10;
            this.label2.Text = "Port Tests (Only if NO active machines)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Controls.Add(this.lblNabUpdate, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.lblNabCom, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.label13, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label12, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.lblPOSLocal, 0, 1);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(196, 198);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(188, 36);
            this.tableLayoutPanel7.TabIndex = 11;
            // 
            // lblNabUpdate
            // 
            this.lblNabUpdate.AutoSize = true;
            this.lblNabUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNabUpdate.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblNabUpdate.Location = new System.Drawing.Point(127, 18);
            this.lblNabUpdate.Name = "lblNabUpdate";
            this.lblNabUpdate.Size = new System.Drawing.Size(58, 18);
            this.lblNabUpdate.TabIndex = 5;
            this.lblNabUpdate.Text = "2600";
            this.lblNabUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNabCom
            // 
            this.lblNabCom.AutoSize = true;
            this.lblNabCom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNabCom.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblNabCom.Location = new System.Drawing.Point(65, 18);
            this.lblNabCom.Name = "lblNabCom";
            this.lblNabCom.Size = new System.Drawing.Size(56, 18);
            this.lblNabCom.TabIndex = 4;
            this.lblNabCom.Text = "9999";
            this.lblNabCom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(127, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 18);
            this.label13.TabIndex = 3;
            this.label13.Text = "Outgoing";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(65, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 18);
            this.label12.TabIndex = 2;
            this.label12.Text = "Outgoing";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 18);
            this.label10.TabIndex = 0;
            this.label10.Text = "Local";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPOSLocal
            // 
            this.lblPOSLocal.AutoSize = true;
            this.lblPOSLocal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPOSLocal.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblPOSLocal.Location = new System.Drawing.Point(3, 18);
            this.lblPOSLocal.Name = "lblPOSLocal";
            this.lblPOSLocal.Size = new System.Drawing.Size(56, 18);
            this.lblPOSLocal.TabIndex = 1;
            this.lblPOSLocal.Text = "6200";
            this.lblPOSLocal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnGetNetwork);
            this.flowLayoutPanel1.Controls.Add(this.btnNabSearch);
            this.flowLayoutPanel1.Controls.Add(this.btnNext);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 468);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(387, 35);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // btnGetNetwork
            // 
            this.btnGetNetwork.Location = new System.Drawing.Point(3, 3);
            this.btnGetNetwork.Name = "btnGetNetwork";
            this.btnGetNetwork.Size = new System.Drawing.Size(100, 28);
            this.btnGetNetwork.TabIndex = 0;
            this.btnGetNetwork.Text = "Get Network Info";
            this.btnGetNetwork.UseVisualStyleBackColor = true;
            this.btnGetNetwork.Click += new System.EventHandler(this.btnGetNetwork_Click);
            // 
            // btnNabSearch
            // 
            this.btnNabSearch.Location = new System.Drawing.Point(109, 3);
            this.btnNabSearch.Name = "btnNabSearch";
            this.btnNabSearch.Size = new System.Drawing.Size(123, 28);
            this.btnNabSearch.TabIndex = 1;
            this.btnNabSearch.Text = "Find Installed Devices";
            this.btnNabSearch.UseVisualStyleBackColor = true;
            this.btnNabSearch.Click += new System.EventHandler(this.btnNabSearch_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(238, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(94, 28);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Next Gateway";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.lstActiveDevices, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.lstOpen, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lstActive, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 278);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.84861F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.15139F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(393, 187);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "Open IPs";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(120, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "Active IPs";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(237, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 24);
            this.label9.TabIndex = 2;
            this.label9.Text = "NAB IP | Merch ID";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstActiveDevices
            // 
            this.lstActiveDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstActiveDevices.FormattingEnabled = true;
            this.lstActiveDevices.Location = new System.Drawing.Point(237, 27);
            this.lstActiveDevices.Name = "lstActiveDevices";
            this.lstActiveDevices.Size = new System.Drawing.Size(153, 157);
            this.lstActiveDevices.TabIndex = 5;
            this.lstActiveDevices.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LstActiveDevices_MouseDoubleClick);
            // 
            // lstOpen
            // 
            this.lstOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstOpen.FormattingEnabled = true;
            this.lstOpen.HorizontalScrollbar = true;
            this.lstOpen.Location = new System.Drawing.Point(3, 27);
            this.lstOpen.Name = "lstOpen";
            this.lstOpen.Size = new System.Drawing.Size(111, 157);
            this.lstOpen.TabIndex = 6;
            this.lstOpen.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstOpen_MouseDoubleClick);
            // 
            // lstActive
            // 
            this.lstActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstActive.FormattingEnabled = true;
            this.lstActive.Location = new System.Drawing.Point(120, 27);
            this.lstActive.Name = "lstActive";
            this.lstActive.Size = new System.Drawing.Size(111, 157);
            this.lstActive.TabIndex = 7;
            // 
            // progBarNab
            // 
            this.progBarNab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progBarNab.Location = new System.Drawing.Point(3, 529);
            this.progBarNab.Name = "progBarNab";
            this.progBarNab.Size = new System.Drawing.Size(387, 15);
            this.progBarNab.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progBarNab.TabIndex = 5;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNote.Location = new System.Drawing.Point(3, 506);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(387, 20);
            this.lblNote.TabIndex = 6;
            this.lblNote.Text = "Double Click Any Open or NAB IP for details";
            this.lblNote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNote.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(399, 553);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Elavon Installer - beta";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.txtOutput, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(393, 547);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // txtOutput
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.txtOutput, 2);
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(3, 3);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOutput.Size = new System.Drawing.Size(387, 267);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.WordWrap = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel5.SetColumnSpan(this.tableLayoutPanel6, 2);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.btnDetectDrivers, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnShowUSBSteps, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnPrintToLog, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnFindUsb, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.progbarElavon, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.chkUsbHid, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel8, 1, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 276);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 5;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.60606F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.39394F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(387, 268);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // btnDetectDrivers
            // 
            this.btnDetectDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDetectDrivers.Location = new System.Drawing.Point(3, 3);
            this.btnDetectDrivers.Name = "btnDetectDrivers";
            this.btnDetectDrivers.Size = new System.Drawing.Size(187, 44);
            this.btnDetectDrivers.TabIndex = 0;
            this.btnDetectDrivers.Text = "Start Elavon Wizard";
            this.btnDetectDrivers.UseVisualStyleBackColor = true;
            this.btnDetectDrivers.Click += new System.EventHandler(this.btnDetectDrivers_Click);
            // 
            // btnShowUSBSteps
            // 
            this.btnShowUSBSteps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnShowUSBSteps.Location = new System.Drawing.Point(196, 3);
            this.btnShowUSBSteps.Name = "btnShowUSBSteps";
            this.btnShowUSBSteps.Size = new System.Drawing.Size(188, 44);
            this.btnShowUSBSteps.TabIndex = 1;
            this.btnShowUSBSteps.Text = "Display USB-HID Steps";
            this.btnShowUSBSteps.UseVisualStyleBackColor = true;
            this.btnShowUSBSteps.Click += new System.EventHandler(this.btnShowUSBSteps_Click);
            // 
            // btnPrintToLog
            // 
            this.btnPrintToLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrintToLog.Location = new System.Drawing.Point(196, 53);
            this.btnPrintToLog.Name = "btnPrintToLog";
            this.btnPrintToLog.Size = new System.Drawing.Size(188, 27);
            this.btnPrintToLog.TabIndex = 3;
            this.btnPrintToLog.Text = "Print Log To File";
            this.btnPrintToLog.UseVisualStyleBackColor = true;
            this.btnPrintToLog.Click += new System.EventHandler(this.btnPrintToLog_Click);
            // 
            // btnFindUsb
            // 
            this.btnFindUsb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFindUsb.Location = new System.Drawing.Point(3, 53);
            this.btnFindUsb.Name = "btnFindUsb";
            this.btnFindUsb.Size = new System.Drawing.Size(187, 27);
            this.btnFindUsb.TabIndex = 4;
            this.btnFindUsb.Text = "Find USB Devices";
            this.btnFindUsb.UseVisualStyleBackColor = true;
            this.btnFindUsb.Click += new System.EventHandler(this.btnFindUsb_Click);
            // 
            // progbarElavon
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.progbarElavon, 2);
            this.progbarElavon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progbarElavon.Location = new System.Drawing.Point(3, 247);
            this.progbarElavon.Name = "progbarElavon";
            this.progbarElavon.Size = new System.Drawing.Size(381, 18);
            this.progbarElavon.TabIndex = 2;
            // 
            // chkUsbHid
            // 
            this.chkUsbHid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.chkUsbHid.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.chkUsbHid, 2);
            this.chkUsbHid.Location = new System.Drawing.Point(101, 86);
            this.chkUsbHid.Name = "chkUsbHid";
            this.chkUsbHid.Size = new System.Drawing.Size(185, 23);
            this.chkUsbHid.TabIndex = 5;
            this.chkUsbHid.Text = "Devices Are USB-HID Configured";
            this.chkUsbHid.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.btnEMVInstall, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(196, 115);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel6.SetRowSpan(this.tableLayoutPanel8, 2);
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(188, 126);
            this.tableLayoutPanel8.TabIndex = 6;
            // 
            // btnEMVInstall
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.btnEMVInstall, 2);
            this.btnEMVInstall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEMVInstall.Location = new System.Drawing.Point(3, 66);
            this.btnEMVInstall.Name = "btnEMVInstall";
            this.btnEMVInstall.Size = new System.Drawing.Size(182, 31);
            this.btnEMVInstall.TabIndex = 0;
            this.btnEMVInstall.Text = "EMV/Converage*";
            this.btnEMVInstall.UseVisualStyleBackColor = true;
            this.btnEMVInstall.Click += new System.EventHandler(this.btnEMVInstall_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label11, 2);
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(3, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(182, 26);
            this.label11.TabIndex = 1;
            this.label11.Text = "*Only as Directed. Not for troubleshooting Elavon";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel8.SetColumnSpan(this.tableLayoutPanel9, 2);
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.lblConverageStatus, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.14035F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.85965F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(182, 57);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // lblConverageStatus
            // 
            this.lblConverageStatus.AutoSize = true;
            this.lblConverageStatus.BackColor = System.Drawing.Color.Green;
            this.lblConverageStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel9.SetColumnSpan(this.lblConverageStatus, 2);
            this.lblConverageStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblConverageStatus.ForeColor = System.Drawing.Color.White;
            this.lblConverageStatus.Location = new System.Drawing.Point(3, 31);
            this.lblConverageStatus.Name = "lblConverageStatus";
            this.lblConverageStatus.Size = new System.Drawing.Size(176, 26);
            this.lblConverageStatus.TabIndex = 0;
            this.lblConverageStatus.Text = "ConvergeConnect Is Running";
            this.lblConverageStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConverageStatus.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(407, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printToFileToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.programToolStripMenuItem.Text = "Menu";
            // 
            // printToFileToolStripMenuItem
            // 
            this.printToFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nABEPXToolStripMenuItem,
            this.elavonOutputToolStripMenuItem});
            this.printToFileToolStripMenuItem.Name = "printToFileToolStripMenuItem";
            this.printToFileToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.printToFileToolStripMenuItem.Text = "Print To File";
            // 
            // nABEPXToolStripMenuItem
            // 
            this.nABEPXToolStripMenuItem.Enabled = false;
            this.nABEPXToolStripMenuItem.Name = "nABEPXToolStripMenuItem";
            this.nABEPXToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.nABEPXToolStripMenuItem.Text = "NAB/EPX";
            this.nABEPXToolStripMenuItem.Click += new System.EventHandler(this.nABEPXToolStripMenuItem_Click);
            // 
            // elavonOutputToolStripMenuItem
            // 
            this.elavonOutputToolStripMenuItem.Enabled = false;
            this.elavonOutputToolStripMenuItem.Name = "elavonOutputToolStripMenuItem";
            this.elavonOutputToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.elavonOutputToolStripMenuItem.Text = "Elavon Output";
            this.elavonOutputToolStripMenuItem.Click += new System.EventHandler(this.elavonOutputToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(133, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 603);
            this.Controls.Add(this.MainTab);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RT CC Quick Installer v1.2.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MainTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl MainTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblGatewayLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblGateway;
        private System.Windows.Forms.Label lblSubnet;
        private System.Windows.Forms.Label lblDns1;
        private System.Windows.Forms.Label lblDns2;
        private System.Windows.Forms.Label lblActiveIPCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnGetNetwork;
        private System.Windows.Forms.Button btnNabSearch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.ProgressBar progBarNab;
        private System.Windows.Forms.ListBox lstActiveDevices;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button btnDetectDrivers;
        private System.Windows.Forms.Button btnShowUSBSteps;
        private System.Windows.Forms.ProgressBar progbarElavon;
        private System.Windows.Forms.Button btnPrintToLog;
        private System.Windows.Forms.Button btnFindUsb;
        private System.Windows.Forms.CheckBox chkUsbHid;
        private System.Windows.Forms.ListBox lstOpen;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.ListBox lstActive;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nABEPXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elavonOutputToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label lblNabUpdate;
        private System.Windows.Forms.Label lblNabCom;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblPOSLocal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button btnEMVInstall;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label lblConverageStatus;
    }
}

