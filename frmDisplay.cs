﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTCCInstaller
{
    public partial class frmDisplay : Form
    {
        public frmDisplay(string[] displaydata)
        {
            this.Data = displaydata;
            InitializeComponent();
        }

        public string[] Data { get; private set; }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Dispose();
        }

        private void frmDisplay_Load(object sender, EventArgs e)
        {
            if(Data.Length > 0)
            {
                if (txtDisplay.InvokeRequired)
                {
                    txtDisplay.BeginInvoke((MethodInvoker)delegate ()
                    {
                        txtDisplay.Clear();
                        txtDisplay.Lines = this.Data;
                        txtDisplay.AppendText(Environment.NewLine);
                        txtDisplay.Invalidate();
                    });
                }
                else
                {
                    txtDisplay.Clear();
                    txtDisplay.Lines = this.Data;
                    txtDisplay.AppendText(Environment.NewLine);
                    txtDisplay.Invalidate();
                }
            }
        }

        private void btnCopyToCb_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtDisplay.Text);
        }
    }
}
